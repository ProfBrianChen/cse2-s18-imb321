import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class DrawPoker{

  public static void main(String[] args){

    int[] cards = new int[52]; //create deck 
    for (int i = 0; i < 52; i++){
      cards[i] = i;

    }

    shuffle(cards); //shuffeing deck 
    int[] p1 = new int[5]; //give deck of cards to player one 
    int[] p2 = new int[5]; // *** same to p2

    for (int i = 0; i < 5; i++){
      p1[i] = cards[2*i];
      p2[i] = cards[2*i + 1];
    }

    System.out.println("Player 1:  ");
    for (int i : p1){
      System.out.print(i + " ");
    }

    System.out.println("\n"); 

    System.out.println("Player 2: ");

    for (int i : p2){
      System.out.print(i + " ");

    }
    //winner (compare points)

    int p1Points = 0;

    int p2Points = 0;
     //flushes

    if (flush(p1) ){
      p1Points += 4;
    }
    if (flush(p2) ){
      p2Points += 4;
    }
     // pairs

    if ( pair(p1) ){
      p1Points += 1;
    }

    if (pair(p2) ){
      p2Points += 1;
    }

    //three of kind

    if (threeOfKind(p1) ){
      p1Points += 2;
    }
    if (threeOfKind(p2) ){
      p2Points += 2;
    }
    //full house 
    if (fullHouse(p1) ){
      p1Points += 8;
    }
    if ( fullHouse(p2) ){
      p2Points += 8;
    }

    if (p1Points == p2Points){ //points are equal,get winner thru high card 
      if ( compHighCard(p1, p2) ){
        p1Points += 1;
      }
      else {
        p2Points += 1;
      }
    }

    if (p1Points > p2Points){ //who wins 
      System.out.println("\nWinner: PLAYER ONE");
    }
    else {
      System.out.println("\nWinner: PLAYER TWO");
    }

  }
  public static void shuffle(int[] input){
    for (int i = 0; i < input.length; i++){
      int randomNum = (int) (Math.random() * input.length);
      int x = input[i];
      input[i] = input[randomNum];
      input[randomNum] = x;
    }

  }
   public static boolean flush(int[] hand){
    int counter = 0;
    for (int i = 1; i < hand.length; i++){
      if (hand[0] / 13 == hand[i] / 13){
        counter += 1;
      }
    }
    if(counter == 4){
      return true;
    }
   return false;
  }
  
  public static boolean pair(int[] hand){
    for (int i = 1; i < hand.length; i++) {
      if (hand[i] % 13 == hand[0] % 13 ) {
        return true;
      }

    } 
    return false;
  }

  public static boolean threeOfKind(int[] hand){
    for (int i = 1; i < hand.length; i++){
      for (int k = i + 1; k < hand.length; k++){
        if (hand[0] % 13 == hand[i] % 13 && hand[0] % 13 == hand[k] % 13 ){
          return true;
        }
      }
    } 
    return false;
  }

  public static boolean fullHouse(int[] hand){ //method for full house , int [] is array input 

    if(pair(hand) && threeOfKind(hand) ){
      return true;
    }
    return false;
  }

 public static boolean compHighCard(int[] hand1, int[] hand2){
    int maxHand1 = hand1[0];
    for (int i = 0; i < hand1.length; i++){
      if (hand1[i] > maxHand1) {
        maxHand1 = hand1[i];
      }
    }
    int maxHand2 = hand2[0];
    for (int i = 0; i < hand2.length; i++){
      if (hand2[i] > maxHand2){
        maxHand2 = hand2[i];
      }
    }
    
    if (maxHand1 > maxHand2){
      return true;
    }
    return false;
  }

}