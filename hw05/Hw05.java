import java.util.Scanner;
public class Hw05{
  public static void main(String[]arg){
   Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter course number:");
    while(!myScanner.hasNextInt()){
      System.out.println("error please enter valid course number: ");
      myScanner.next();
    }
    int courseNumber = myScanner.nextInt();
  
    System.out.println("Enter department name");
    while(!myScanner.hasNext()){
      System.out.println("error enter correct department name: ");
    }
    String depName = myScanner.next();
    
    System.out.println("Enter number of times you meet for class");
    while(!myScanner.hasNextInt()){
      System.out.println("enter correct number! ");
      myScanner.nextInt();
    }
    int numberTimes = myScanner.nextInt();
    
    System.out.println("The time the class starts");
    while(!myScanner.hasNextInt()){
      System.out.println("error enter correct time lala");
      myScanner.nextInt();
    }
    int time = myScanner.nextInt();
    
    System.out.println("Enter teacher name:");
    while(!myScanner.hasNext()){
      System.out.println("error enter correct name :");
      myScanner.next();
    }
    String nameTeacher = myScanner.next();
    
    System.out.println("how many kiddos in the class?");
    while(!myScanner.hasNext()){
      System.out.println("error enter correct amount of kiddos");
      myScanner.nextInt();
    }
    int numKids = myScanner.nextInt();
    
    System.out.println("Course number: " + courseNumber);
    System.out.println("Department Name: " + depName);
    System.out.println("Number of times you meet for class " + numberTimes);
    System.out.println("Start Time: " + time);
    System.out.println("Professor Name: " + nameTeacher);
    System.out.println("Number of students: " + numKids);
  }
}