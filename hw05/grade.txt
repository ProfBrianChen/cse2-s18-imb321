Grade:  90/100

Comments:
A) Does the code compile?  How can any compiler errors be resolved?
Both programs compile correctly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
The program runs however it stops when a letter is entered instead of a number it stops running 
( entering the number of times the class meets example).
Also, no error is shown when a number is inputed for a name. 
C) How can any runtime errors be resolved?
Make sure when using loops that they ask the user for another input to scan in. Only some loops were implemented correctly. 
D) What topics should the student study in order to avoid the errors they made in this homework?
Try to rework this homework to make loops correctly work. 
E) Other comments:
Good job on this! There are however no comments, these need to be added to the next homework. 
