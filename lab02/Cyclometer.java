//Izzy Brennan 2/2/18 CSE2 Arthimetic Calculations
// Document your program. What does MPG do? Place you comments here!
//miles per gallon 
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

	 //end of main method   
 
int secsTrip1=480;  // trip 1 took 480 seconds
int secsTrip2=3220;  // trip 2 took 3220 seconds
int countsTrip1=1561;  // wheeel during trip one rotated 1561 counts
int countsTrip2=9037; //  wheel during trip 2 rotated 9037 counts 
Double wheelDiameter=27.0;  // wheel diameter is 27.0 
Double Pi=3.14159; // the numeric value of PI
int FeetPerMile=5280;  // interger vaule for feet per mile
int InchesPerFoot=12;   // interger value for inches per foot
int SecondsPerMinute=60;  // interger value of seconds per min
Double distanceTrip1, distanceTrip2,totalDistance;  // creating variables to define distance of two trips and then total distance of both 
System.out.println("Trip 1 took "+
       	     (secsTrip1/SecondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
System.out.println("Trip 2 took "+
       	     (secsTrip2/SecondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

distanceTrip1=countsTrip1*wheelDiameter*Pi;
    	
distanceTrip1/=InchesPerFoot*FeetPerMile; // Gives distance in miles
distanceTrip2=countsTrip2*wheelDiameter*Pi/InchesPerFoot/FeetPerMile;
totalDistance=distanceTrip1+distanceTrip2;
	
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");
  }
}
 

