import java.util.Random;
import java.util.Arrays;
public class RobotCity{
   public static void main(String[]args){
     //call other methods 
    int[][] population = buildCity(); //array named population, buildCity = method that creates grid
     for(int i = 0; i<5; i++){ //loop runs entire process (each method) five times
      display(population);//display method to show population wihtin each city coordinate 
       System.out.println();
    int numberOfRobots = (int)(Math.random() * 5);// random generator for # of robots invading
     invade(population, numberOfRobots); // calling invade method , both elements go into [][]
     display(population); 
      System.out.println();
     update(population);
     display(population);
       System.out.println();
     }
     }
   
  
    public static int[][] buildCity() { //city population 
  //blocks 
   int y = (int)(Math.random()*(16-10))+10; //coordinates west to east
   int x = (int)(Math.random()*(16-10))+10; //coordinates north to south 
   int[][] cityArray = new int[y][x]; //do the for loops follow the order y,x?
   for(int i=0; i<cityArray.length; i++){ //rows 
     for(int j = 0; j<cityArray[i].length; j++){
      cityArray[i][j] =(int)(Math.random()*(1000-100)) + 100; //array is random int from 100-999
 }
 }
 return cityArray;     
  }
  
public static void display (int[][] population){
   for(int i = 0; i<population.length; i++){
       for(int j = 0; j<population[i].length; j++){
         System.out.printf("%6d", population[i][j]);
       }
     System.out.println();
}
}
public static void invade(int[][] city, int numberOfRobots){ // int[population][numberOfRobots]
for(int i = 0; i<numberOfRobots; i++){ //loop 
  int y = (int)(Math.random()*(city.length)); // generates random row
  int x = (int)(Math.random()*(city[0].length)); //generates where it lands somewhere within that row
  if(city[y][x] >= 0){ //if one lands on a negative spot (meaning one is already there) then
    city[y][x]*=-1; //makes the coordiante pos 
  }
}
}
public static void update(int[][] city){ //int[population][????]
  for(int i = 0; i< city.length; i++){ //runs through 
    int[] currentRow = city[i]; // one demonsional array pointing to the current row that is indicated by random int 
    // of the city.length (method calling population & .... info)??
    
    //for very last collum, cant move robot to the right (off grid) so special case:
    if(currentRow[currentRow.length - 1] <= 0) { //if currentRow array of last row in current array is negative then:
      currentRow[currentRow.length - 1] *= -1;//multiply that spot by negative one to make pos 
    }
    for(int j = currentRow.length-2; j >= 0; j--){ //exlcuding the last collumn (-2); regualr loop for moving over 
      //from right to left 
      if(currentRow[j] < 0){ //if current row is higher than 0 
        currentRow[j+1]*= -1;// 
        currentRow[j]*=-1;//
      }
      
    }
  }
}  
  
  
  
}
