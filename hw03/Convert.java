import java.util.Scanner;
public class Convert{
  public static void main(String[] args) {
   Scanner myScanner = new Scanner ( System.in ); // intro for java 
   System.out.print("Enter affected area (in acres form):"); //input
   double Area = myScanner.nextDouble(); //set double
   System.out.print("Enter rainfall in affected area in inches"); //input
   double Rain = myScanner.nextDouble(); // double set
   double inches = (Rain * Area); //equaton for acre (in inches)
   double cubicM = (inches / 40550400); //conversion to cublic miles 
   System.out.println("Total rain fall in (cubic miles) is :" + cubicM); //prints total rain fall in cubic miles 
   
  }
}