Grade:  100/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
File compiles perfectly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Code runs perfectly.
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Good job on this but you are missing comments at the beginning with your name, date and assignment