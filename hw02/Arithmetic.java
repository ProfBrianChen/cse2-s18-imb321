public class Arithmetic {
   public static void main(String[] args) {
      //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per box of envelopes
double beltPrice = 33.99;

//the tax rate

double paSalesTax = 0.06;

     
     
double totalCostOfPants = numPants*pantsPrice;   //total cost of pants 
double totalCostOfPantsTax = totalCostOfPants + (totalCostOfPants*paSalesTax);
double totalCostOfShirts = numShirts*shirtPrice;
double totalCostOfShirtsTax = totalCostOfShirts + (totalCostOfShirts*paSalesTax);
double totalCostOfBelts = numBelts*beltPrice;
double totalCostOfBeltsTax = totalCostOfBelts + (totalCostOfBelts*paSalesTax);
double CostBeforeTax = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;
double CostAfterTax = totalCostOfBeltsTax + totalCostOfShirtsTax + totalCostOfPantsTax;
     
     
System.out.printf("total cost for pants is $%10.2f \n" , (totalCostOfPants));
System.out.printf("total cost for shirts is $%10.2f \n", (totalCostOfShirts));
System.out.printf("total cost for belts is $%10.2f \n", (totalCostOfBelts));
System.out.printf("total cost for belts with tax is $%10.2f \n", (totalCostOfBeltsTax));
System.out.printf("total cost for shirts with tax is $%10.2f \n", (totalCostOfShirtsTax));
System.out.printf("total cost for pants with tax is $%10.2f \n", (totalCostOfPantsTax));
     
System.out.printf("total cost before tax is $%10.2f \n", (CostBeforeTax));
System.out.printf("total cost after tax is $%10.2f \n", (CostAfterTax));
  
   
   }
 }


