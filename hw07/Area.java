import java.util.Scanner;
public class Area{
  //first method asking for shape 
    public static String Shape(){
     Scanner myScanner = new Scanner (System.in);
     System.out.println("Enter rectangle triangle or circle: ");
     String shape = myScanner.next();
     boolean shapeA = true; 
      
      
     while (shapeA){
       if(shape.equals ("rectangle")){
         break;
       }
       else if (shape.equals ("triangle")){
         break;
       }
       else if (shape.equals ("circle")){
         break;
       }
       else{
         System.out.println("Please enter rectangle triangle or circle");
         shape = myScanner.next();}
       }
      return shape;
     }
    
  
  // method rectangle 
  public static double rectangle(){
  Scanner myScanner = new Scanner (System.in);
      System.out.println("Enter length of rectangle in double form ");
    while (!myScanner.hasNextDouble()){
      System.out.println("Error, enter in form of a double please ");
      String errorInput = myScanner.nextLine();
    }
      double length = myScanner.nextDouble();
    System.out.println("Enter width of rectangle in form of a double ");
    while (!myScanner.hasNextDouble()){
      System.out.println("Error, enter in form of a double please ");
      String errorInput = myScanner.nextLine();
    }
    double width = myScanner.nextDouble();
    double area = length*width;
    return area;
  }
  
  
  //method cirlce
    public static double circle (){
  Scanner myScanner = new Scanner (System.in); 
  double Pi = 3.14159265359;
  System.out.println("Enter width of circle in form of a double ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter in form of a double please ");
    String error = myScanner.nextLine();
  }
  double radius = myScanner.nextDouble();
  double area = (Pi * radius * radius);
  return area;
}
  
  
  //triangle method
  
  public static double triangle (){
  Scanner myScanner = new Scanner (System.in); 
  System.out.println("Please enter a double value to define the base of the triangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String error = myScanner.nextLine();
  }
    double base = myScanner.nextDouble();
  System.out.println("Please enter a double value to define the height of the triangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String error = myScanner.nextLine();
  }
  double height = myScanner.nextDouble();
  double area = ((.5) * base * height);
  return area;
  }
  
  //main method
  public static void main(String[] args){
    String shape = Shape();
    double area = 0;
      if(shape.equals("rectangle")){
        area = rectangle();
      } 
      else if (shape.equals("triangle")){
        area = triangle();
      } 
      else if (shape.equals("circle")){
        area = circle();
      }
    System.out.println("Area of the " + shape + " is: " + area);
  } 
}
