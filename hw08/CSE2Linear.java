import java.util.Arrays;
import java.util.Scanner;
public class CSE2Linear{
  public static void main(String[] args){ //main method
    System.out.println("enter 15 grades from least to greatest, 0 to 100");
    Scanner myScanner = new Scanner(System.in);
    int counter = 0;
    
    int [] arrayA = new int[15]; 
    while( counter < 15){
      if(myScanner.hasNextInt()){
        arrayA[counter] = myScanner.nextInt(); //saves it to list 
        if(arrayA[counter] < 0 || arrayA[counter] > 100){
          System.out.println("you must enter an int within correct range ");
          System.exit(0);
        }
        else{
          if(counter > 0 ){
            if(arrayA[counter] < arrayA[counter - 1]){ //check to see if goes from least to greastes throught out 
              System.out.println("you didnt enter from least to greatest!!");
              System.exit(0);
            }
            
          }
          counter++;
        }
    }
      else{
        System.out.println("must enter number");
        System.exit(0);
      }
      
    }
    
   System.out.println("Sorted");
   System.out.println(Arrays.toString(arrayA)); //converts array to string, displays string 
   System.out.println("Enter grade to search for");
    if(myScanner.hasNextInt()){
      int x = myScanner.nextInt(); //if the int was saved
      BinarySearch(arrayA, x); //calls search 
      Scramble(arrayA);
      
      System.out.println("Scrambled:");
      System.out.println(Arrays.toString(arrayA));
      System.out.println("enter grade to search for");
      
        if(myScanner.hasNextInt()){
          LinearSearch(arrayA, x); //if calls to linear method
          
        }
        else{
          System.out.println("didnt enter a number ");
          System.exit(0);
        }
      
      
        
    }
    else{
      System.out.println("you didnt enter a grade");
      System.exit(0);
    }
    
    

}
  public static void LinearSearch(int[] arrayA, int x){ //method for LINEAR SEARCH
    for( int i = 0; i < arrayA.length; i++){
      if(arrayA[i] == x){ //see if number enter is equal to number at given postion 
        System.out.println(x + " "+  "was found in list"+ " " + (i+1)+ " "+  "iterations");
        break;
        
      }
      else if(arrayA[i] != x && i == (arrayA.length - 1)){
        //if input wasnt found after # of iterations (15)
        System.out.println( x + "  "+  "was not found in" + " "+ (i +1) + " "+  "iterations");
        
    }
      
    
  }
                                 
}
  
  public static int[] Scramble(int[] arrayA){ // methods for SCRAMBLE 
    for(int i = 0; i < arrayA.length; i++){
      int x = (int) (Math.random()*arrayA.length); //radndom num generator 
      int save = arrayA[i];
      while( x != i){ //swtiches i to random number if x and i are not equal 
        arrayA[i] = arrayA[x];
        arrayA[x] = save; 
        break;
        
      }
      
    }
    return arrayA;
  }
  
  public static void BinarySearch(int[] arrayB, int integer){ //Binary Search Method 
    int highIndex = arrayB.length -1 ; //establishing indexes and counter 
    int lowIndex = 0;
    int counter = 0;
    while(lowIndex <= highIndex){
      counter++;
      int midIndex = (lowIndex + highIndex) / 2;
      if(integer < arrayB[midIndex]){
        highIndex = midIndex - 1;
      }
      else if(integer > arrayB[midIndex]){
        lowIndex = midIndex +1;
      }
      else if( integer == arrayB[midIndex]){
        System.out.println(integer + ""+  "was found" + " " +  counter + " "+  "iterations");
        break;
      }
      
    }
    if(lowIndex > highIndex){
      System.out.println(integer + " "+  "was NOT found" + " " + counter + " " +  "iterations");
    }
    
  }
  
}
  
  
  
  
  
  
  
  
  
  
  