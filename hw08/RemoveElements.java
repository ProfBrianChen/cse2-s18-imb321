import java.util.Scanner;
public class RemoveElements{

  public static void main(String [] arg)// main method 
  {
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArrayA[];
    int newArrayB[];
    int index,target;
  	String answer="";
  	do{
  	  System.out.print("Random input 10 ints [0-9]");
    	num = randomInput();
    	String out = "The original array is:";
    	out += listArray(num);
    	System.out.println(out);
    	
      System.out.print("Enter the index ");
      index = scan.nextInt();
      if (index < 0 || index > num.length - 1){
      
        System.out.println("Your number was not in the range 0-9");
        System.out.println(out);
      }
      else{ 
      newArrayA = delete(num,index);
    	String out1="The output array is ";
      out1+=listArray(newArrayA);  
      System.out.println(out1);
      }
   
      System.out.print("Enter the target value ");
    	target = scan.nextInt();
    	newArrayB = remove(num,target);
    	String out2="The output array is ";
    	out2+=listArray(newArrayB);
    	System.out.println(out2);
        	 
    	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
    	answer=scan.next();
  	}
      while(answer.equals("y"));
  }
   
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
    	if(j>0){
    	
        out+=", ";
    	}
  	  out+=num[j];
    }
    out+="} ";
    return out;
  }
  
    public static int[] remove(int[]array, int y){
    int counter = 0;
    for (int i = 0; i < array.length; i++){
    
      if (array[i] != y){
      
        counter++;
      }
    }
    int array2[] = new int [counter];
    int counter2 = 0;
    for (int j = 0; j < array.length; j++){
      if (array[j] != y){
      
        array2[counter2] = array[j];
        counter2++;
      }
    }
    return array2;
  }
  
  public static int[] randomInput(){
    int num[]=new int[10];
    for (int i = 0; i < num.length; i++){
    
      int x = (int)(Math.random()*10);
      num[i] = x;
    }
    return num;
  }
  
  public static int[] delete(int[]array, int y){
    int [] array2 = new int[9];
    for (int j = 0; j < y; j++)
    {
      array2[j] = array[j];
    }
    for (int i = y + 1; i < array.length; i++){
    
      array2[y] = array[i];
      y++;
    }
    return array2;
  }
  

}